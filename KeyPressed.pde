void keyPressed() {
    //trova keyCode
    //println("CODICE: " + keyCode);
    
    if(keyCode == 71){
      creaCelle();
    }
    
    //QUANDO PREMO r o R parte il ciclo della rosoluzione del sudoku
    if(keyCode == 82 || keyCode == 16){
      risolviSudoku();
    }
    
    //QUANDO PREMO ENTER o SPAZIO confermo il numero nella cella selezionata 
    if(keyCode == 10 || keyCode == 32){
      for(int r = 0; r < cellePerLato; r++){
        for(int c = 0; c < cellePerLato; c++){
          celle[r][c].setSelezionata(false);
          UCS.setSelezionata(false);
        }
      }
    }
    
    //QUANDO PREMO DEL o CANC confermo il numero nella cella selezionata 
    if(keyCode == 8 || keyCode == 127){
      for(int r = 0; r < cellePerLato; r++){
        for(int c = 0; c < cellePerLato; c++){
          if(celle[r][c].isSelezionata()) celle[r][c].setN(0);
          if(celle[r][c].isSelezionata()) celle[r][c].setFissa(false);
        }
      }
    }
    
    //QUANDO PREMO TAB togglo lo stato fisso della cella selezionata
    if(keyCode == 9){
      for(int r = 0; r < cellePerLato; r++){
        for(int c = 0; c < cellePerLato; c++){
          if(celle[r][c].isSelezionata()) celle[r][c].toggleFissa();
        }
      }
    }
    
    //QUANDO PREMO ESC parte la chiusura del programma nel draw
    if(keyCode == 27){
      exit();
    }
    
    //QUANDO PREMO FRECCIA SX 37, DX 39, SU 38, GIU 40 se la cella non è stata messa fissa
    if(keyCode == 37 || keyCode == 39 || keyCode == 38 || keyCode == 40){
      //println(UCS.getR());
      //println(UCS.getC());
      
      if(keyCode == 37){ //sx
        if(UCS.getR() - 1>= 0){
          celle[UCS.getR()][UCS.getC()].setSelezionata(false); //deseleziono la cella selezionata attualmente
          celle[UCS.getR() - 1][UCS.getC()].setSelezionata(true);
          UCS.setR(UCS.getR() - 1);
        }
      } else if(keyCode == 39){ //dx
        if(UCS.getR() + 1< 9){
          celle[UCS.getR()][UCS.getC()].setSelezionata(false); //deseleziono la cella selezionata attualmente
          celle[UCS.getR() + 1][UCS.getC()].setSelezionata(true);
          UCS.setR(UCS.getR() + 1);
        }
      } else if(keyCode == 38){ //su
        if(UCS.getC() - 1>= 0){
          celle[UCS.getR()][UCS.getC()].setSelezionata(false); //deseleziono la cella selezionata attualmente
          celle[UCS.getR()][UCS.getC() - 1].setSelezionata(true);
          UCS.setC(UCS.getC() - 1);
        }
      } else if(keyCode == 40) { // giu
        if(UCS.getC() + 1 < 9){
          celle[UCS.getR()][UCS.getC()].setSelezionata(false); //deseleziono la cella selezionata attualmente
          celle[UCS.getR()][UCS.getC() + 1].setSelezionata(true);
          UCS.setC(UCS.getC() + 1);
        }
      }
    }
    
    //controllo se ho cliccato un tasto numerico, in tal caso vado a vedere r e c della cella clicata
    if((keyCode >= 48 && keyCode <= 57 || keyCode >= 96 && keyCode <= 105) && (!celle[UCS.getR()][UCS.getC()].isFissa() || celle[UCS.getR()][UCS.getC()].getN() == 0)){
      int rPreSelezionata = - 1;
      int cPreSelezionata = - 1;
      boolean trovata = false;
      
      //deseleziono ogni cella, trovo se ce n era una ancora cliccata e in tal caso salvo num riga e col
      for(int r = 0; r < cellePerLato && !trovata; r++){
        for(int c = 0; c < cellePerLato && !trovata; c++){
          if(celle[r][c].isSelezionata()){
            rPreSelezionata = r;
            cPreSelezionata = c;
            trovata = true;
          }
        }
      }
    
      if(trovata){
        //controllo se premo un tasto per l inserimento di numeri, e se sono tra 0 e 9 li assegno alla cella cliccata
        if(keyCode - 48 <= 9) inserisciNumero(rPreSelezionata,cPreSelezionata, keyCode - 48);
        else inserisciNumero(rPreSelezionata,cPreSelezionata, keyCode - 96);
      }
    }
}