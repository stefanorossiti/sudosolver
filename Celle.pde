private void creaCelle(){
  for(int r = 0; r < cellePerLato; r++){
    for(int c = 0; c < cellePerLato; c++){
      celle[r][c] = new Cella(r, c, lato/cellePerLato);
    }
  }
}

private void risolviSudoku(){
  new thRisolutore(celle);
}

//metodo per l inserimento del numero nella cella, questo metodo controlla se il numero è inseribile
// e lo inserisce, se non lo è ritorna r e c della cella che va in conflitto
private void inserisciNumero(int r, int c, int n){
  //println(c);
  //println(r);
  
  Cella[] cellaConflitto = new Cella[3];
  cellaConflitto[1] = null;
  cellaConflitto[1] = null;
  cellaConflitto[2] = null;
  
  //Controllo in tutta la COLONNA se è già presente il numero che si vuole inserire
  for(int iC = 0; iC < 9; iC++){
    if(n != 0 && n == celle[r][iC].getN()){
      cellaConflitto[0] = new Cella(r, iC, n);
      //println("numero gia presente alla Col: " + r + " e alla rig: " + iC);
      //println("n conf " + celle[c][iC].getN() + " n nuovo: " + celle[c][r].getN());
    }
  }
  
  //Controllo in tutta la RIGA se è già presente il numero che si vuole inserire
  for(int iR = 0; iR < 9; iR++){
    if(n != 0 && n == celle[iR][c].getN()){
      cellaConflitto[1] = new Cella(iR, c, n);
      //println("numero gia presente alla Col: " + iR + " e alla rig: " + c);
      //println("n conf " + celle[c][iR].getN() + " n nuovo: " + celle[c][r].getN());
    }
  }
  
  //Controllo che nella regione non è già presente lo stesso numero
  //trovo la regione della cella
  int cR = floor(r/3);
  int rR = floor(c/3);
    
  //println(rR);
  //println(cR);
  
  for(int i = 0; i <3; i++){
    for(int j = 0; j <3; j++){
      if(n != 0 && n == celle[cR*3 + j][rR*3 + i].getN()){
        //controllo che cella modificata e controllata siano diverse
          if(r != (cR*3 + j) && c != (rR*3 + i)){ //<>//
            cellaConflitto[2] = new Cella(cR*3 + j, rR*3 + i, n);
            
            //println(cR*3 + j + " " + r);
            //println(rR*3 + i + " " + c);
            //println();
          }
          //println(celle[rR*3 + j][cR*3 + i].getN());
      }  
      //println(rR*3 + j + " " + c);
      //println(cR*3 + i + " " + r);
      //println();
    }
  }  
  
  
  if(cellaConflitto[0] != null){
    new ThTiming(cellaConflitto[0].getR(), cellaConflitto[0].getC()).inizia();
  }
  if(cellaConflitto[1] != null){
    new ThTiming(cellaConflitto[1].getR(), cellaConflitto[1].getC()).inizia();
  }
  if(cellaConflitto[2] != null){
    new ThTiming(cellaConflitto[2].getR(), cellaConflitto[2].getC()).inizia();
  }
     
  if(cellaConflitto[0] == null && cellaConflitto[1] == null && cellaConflitto[2] == null){
    celle[r][c].setN(n);
  }
}
