import g4p_controls.*;

int lato = 450; //lato della finestra (quadrato)
int cellePerLato = 9; //quante celle ha un lato
Cella[][] celle = new Cella[cellePerLato][cellePerLato];
Cella UCS = new Cella(0, 0, lato/cellePerLato);; // per riferimento all Ultima Cella Selezionata

void setup() {
  println(dataPath(""));
  
  //chiamo il metodo per il cambio di icona e del titolo
  frameAndIcon("|SudoSolver| Powered by Stefano BBC Rossi",dataPath("") + "\\me.jpg");
  
  size(450, 450);  // Size must be the first statement
  //width(displayWidth);
  //height(displayHeight);
  surface.setSize(width + 5, height + 5);
  frameRate(30);
  //frame.setResizable(true); 
  
  //creo array bidim di celle chiamato tabella
  creaCelle();
}

void draw() {
  background(255);
  //lato = width;
  //frame.setSize(lato, lato);
  stampaTabella();
  stampaCelle();
  stampaWarning();
  stampaNumCelle();
}

private void stampaWarning(){
  for(int r = 0; r < cellePerLato; r++){
    for(int c = 0; c < cellePerLato; c++){
      fill(celle[r][c].getWarningColor()); //rosso in strasparenza
      rect(celle[r][c].getX() + 7, celle[r][c].getY() + 7, celle[r][c].getL() -9, celle[r][c].getL() - 9, 10);
    }
  }
}

private void stampaNumCelle(){
  for(int r = 0; r < cellePerLato; r++){
    for(int c = 0; c < cellePerLato; c++){
      if(celle[r][c].getN() != 0){
        fill(celle[r][c].getColN());
        textSize(celle[r][c].getPntFontN());
        text(str(celle[r][c].getN()), celle[r][c].getX() + 21, celle[r][c].getY() + 37);
      }
    }
  }
}

private void stampaCelle(){
  noStroke();
  
  for(int r = 0; r < cellePerLato; r++){
    for(int c = 0; c < cellePerLato; c++){
      //se la cella è fissa stampo uno sfondo più grigio
      if(celle[r][c].isFissa()){
        fill(100, 0, 0, 40); //grigio
        rect(celle[r][c].getX() + 3, celle[r][c].getY() + 3, celle[r][c].getL(), celle[r][c].getL());
      }
      
      if(celle[r][c].isSelezionata()){
        fill(celle[r][c].getColCella()); //Azzutto
        rect(celle[r][c].getX() + 13, celle[r][c].getY() + 13, celle[r][c].getL() - 20, celle[r][c].getL() - 20, 8);
      }else{
        noFill();
        rect(celle[r][c].getX() + 13, celle[r][c].getY() + 13, celle[r][c].getL() - 20, celle[r][c].getL() - 20, 8);
      }
    }
  }
}

private void stampaTabella(){
  stroke(0);
  
  for(int i = 0; i < 10; i++){
    for(int j = 0; j < 10; j++){
      
      if (j == 0 || j == 3 || j == 6 || j == 9){ 
        if (i == 0 || i == 3 || i == 6 || i == 9) strokeWeight(5);
        else strokeWeight(1);
      }
      else strokeWeight(1);
      
      line(0, (lato/cellePerLato) * i + 2, lato, (lato/cellePerLato) * i + 2);
      
      if (i == 0 || i == 3 || i == 6 || i == 9) strokeWeight(5);
      else strokeWeight(1);
      line((lato/cellePerLato) * i + 2, 0, (lato/cellePerLato) * i + 2, lato + 2);
    }
  }
}
