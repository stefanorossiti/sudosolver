# SudoSolver

## Description

A simple sudoku solver in processing 3

## Instructions

- select any cell with left click, move with arrows
- pres Del to delete any cell content
- press R to solve the sudoku
- the algorithm prevent you to insert a number in a wrong cell

Start with an empty map.

Write some number into the map.

![Scheme](images/main.png)

Press R

![Scheme](images/resolv.png)

Good work!

## Executables

You can find the executables for windows and linux, arm architecture supported: [releases](https://gitlab.com/stefanorossiti/sudosolver/-/releases).

### Executables Requirements

- [Java 7](https://www.oracle.com/java/technologies/javase/javase7-archive-downloads.html)

## Development Requirement

[Processing 3.1+](https://processing.org/download/)  (best) until 3.5.4 should be fine.

## Installation and run

Download processing 3

run it in the IDE

This is a Processing 3 Project. You need the Pframe library to compile and run the code.

- [PFrame](https://github.com/shigeodayo/PFrame)

Extract the library in a separate folder into sketchbook libraries folder:

- Default libraries folder: `<user>\Documents\Processing\libraries`. If it doesn't exist, create it.

## License

All my work is released under [DBAD](https://www.dbad-license.org/) license
